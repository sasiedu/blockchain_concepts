package main

import (
	"crypto/elliptic"
	"crypto/rand"
	"fmt"
	"math/big"

	cp "./curvePoint"
)

var zkCurve elliptic.Curve

func generateSecretValue() *big.Int {
	minSecretValue := big.NewInt(9999999999)
	maxRandomValue := big.NewInt(999999999999)
	secretValue := big.NewInt(0)
	for secretValue.Cmp(minSecretValue) < 1 {
		randomValue, _ := rand.Int(rand.Reader, maxRandomValue)
		secretValue = secretValue.Add(secretValue, randomValue)
	}
	return secretValue
}

func generateRandomValue() *big.Int {
	maxRandomValue := big.NewInt(999999999999)
	randomValue, _ := rand.Int(rand.Reader, maxRandomValue)
	return randomValue
}

func printCurve() {
	fmt.Println("Curve name: ", zkCurve.Params().Name)
	fmt.Println("Curve field, P: ", zkCurve.Params().P)
	fmt.Println("Curve order, N: ", zkCurve.Params().N)
	fmt.Println("Curve constant, B: ", zkCurve.Params().B)
	fmt.Println("Curve base point, G: (", zkCurve.Params().Gx, ", ", zkCurve.Params().Gy, ")")
	fmt.Println("Curve bit size: ", zkCurve.Params().BitSize)
}

func proverGenerateSecretCommitment(secret *big.Int) *cp.CurvePoint {
	secretCommitment := &cp.CurvePoint{zkCurve.Params().Gx, zkCurve.Params().Gy}
	secretCommitment = secretCommitment.ScalarMult(zkCurve.Params(), secret.Bytes())
	return secretCommitment
}

func proverGenerateRandomCommitment() (*big.Int, *cp.CurvePoint) {
	randomValue := generateRandomValue()
	randomCommitment := &cp.CurvePoint{zkCurve.Params().Gx, zkCurve.Params().Gy}
	randomCommitment = randomCommitment.ScalarMult(zkCurve.Params(), randomValue.Bytes())
	return randomValue, randomCommitment
}

func verifierGenerateRandomValue() *big.Int {
	randomValue := generateRandomValue()
	return randomValue
}

func proverCalcVerifierRandomNumber(secret, verifierRandomValue, proverRandomValue *big.Int) *big.Int {
	result := secret.Mul(secret, verifierRandomValue)
	result = result.Add(result, proverRandomValue)
	return result
}

func verifierValidatesResult(proverAnswer, verifierRandomValue *big.Int, randomCommitment, secretCommitment *cp.CurvePoint) bool {
	generatorPoint := &cp.CurvePoint{zkCurve.Params().Gx, zkCurve.Params().Gy}
	resultLHS := generatorPoint.ScalarMult(zkCurve.Params(), proverAnswer.Bytes())
	resultRHS := secretCommitment.ScalarMult(zkCurve.Params(), verifierRandomValue.Bytes())
	resultRHS = resultRHS.Add(zkCurve.Params(), randomCommitment)
	fmt.Println("resultLHS:", resultLHS.String())
	fmt.Println("resultRHS:", resultRHS.String())
	return (resultLHS.X.Cmp(resultRHS.X) == 0) && (resultLHS.Y.Cmp(resultRHS.Y) == 0)
}

func main() {
	// prover's private key
	secret := generateSecretValue()
	fmt.Println("Secret value: ", secret.String())
	zkCurve = elliptic.P256()
	printCurve()
	// prover's public key
	proverSecretCommitment := proverGenerateSecretCommitment(secret)
	fmt.Println("Secret Commitment:", proverSecretCommitment.String(), "is on curve", zkCurve.IsOnCurve(proverSecretCommitment.X, proverSecretCommitment.Y))
	proverRandomValue, proverRandomCommitment := proverGenerateRandomCommitment()
	fmt.Println("Prover Random Value:", proverRandomValue)
	fmt.Println("Prover Random Commitment:", proverRandomCommitment.String(), "is on curve", zkCurve.IsOnCurve(proverRandomCommitment.X, proverRandomCommitment.Y))
	verifierRandomValue := verifierGenerateRandomValue()
	fmt.Println("Verifier Random Value:", verifierRandomValue)
	proverAnswer := proverCalcVerifierRandomNumber(secret, verifierRandomValue, proverRandomValue)
	fmt.Println("Prover's Answer to verifier's number:", proverAnswer)
	isTrue := verifierValidatesResult(proverAnswer, verifierRandomValue, proverRandomCommitment, proverSecretCommitment)
	fmt.Println("Verifier says", isTrue)
}
