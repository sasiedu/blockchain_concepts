
# Zero Knowledge Proof Grandmaster problem

## Problem (attack)
```txt
    Here's how Alice, who doesn't even know the rules to chess, can defeat a grandmaster. She challenges both Gary Kasparov and Magnus Carlsen to a game, at the same time and place, but in separate rooms. She plays white against Kasparaov and black against Carlsen. Neither Grandmaster knows about the other.

    Carlsen, as white, makes his first move. Alice records the move and walks into the room with Kasparaov. Playing white, she makes the same move against Kasparov. Kasparov makes his first move as black. Alice records the move, walks into the room with Carlsen, and makes the same move. This continues, until she wins one game and loses the other, or both games end in a draw.

    In reality, Kasparov is playing Carlsen and Alice is simply acting as the middleman, mimicking the moves of each grandmaster on the other's board. However, if neither Carlsen nor Kasparov knows about the other's presence, each will be impressed will Alice's play.

    This kind of fraud can be used against zero-knowledge proofs of identity. While Alice is proving her identity to Mallory, Mallory can simultaneously prove to Bob that she is Alice.
```

## Problem (not attack)
```txt
However, we do not consider this to be a real attack, because the "union" of the two "sessions" is a single session in which Alice has successfully identified herself to Bob. The overall result is that Bob issued a challenge r and Alice computed the correct response y to the challenge. 
Oscar simply forwarded messages to their intended recipients without modifying the scheme. The session executed exactly as it would have if Oscar had not been present.

A clear formulation of the adversarial goal should allow us to demonstrate that this is not an attack. We adopt the following approach. We will define the adversary (Oscar) to be active in a particular session if one of the following conditions holds:

    Oscar creates a new message and places it in the channel
    Oscar changes a message in the channel, or
    Oscar diverts a message in the channel so it is sent to someone other than the intended receiver.

The goal of the adversary is to have the initiator of the scheme (e.g., Bob, who is assumed to be honest) "accept" in some session in which the adversary is active. According to this definition, Oscar is not active in the intruder-in-the-middle scenario above, and therefore the adversarial goal is not realized.
```

## Above is only an attack if
```txt
Alice sends a message to Bob, containing her certificate, of her intent to authenticate.
Bob verifies Alice's certificate, randomly chooses number t, and computes x≡t2(modn). He then sends x to Alice.
Alice receives x from Bob, computes y≡x−−√(modn), and sends y back to Bob.
Oscar hijacks the message, and sends −y to Bob. Bob confirms that −y2≡x(modn), and accepts.
```

## Solution
```txt
The motivation, to me, is that in reality you can consider any router on the internet to be successfully executing an "intruder-in-the-middle" attack just by forwarding messages unchanged. After a successful execution of the identification scheme, Bob knows that someone on the channel is Alice, which is all the protocol was hoping to achieve. It was originally designed for smart cards, where the channel was guaranteed to be secure, but the person on the other end was unknown. When using it in a situation with potential man-in-the-middle attackers, you don't know that the channel is secure without additional safeguards.

In reality, you need to combine the identification scheme with a secure channel. You can do this by executing a key exchange (Diffie-Hellman for instance), and then using something from that exchange to "bind" the secure channel and your authentication scheme together. The value that you use and how you bind it will depend on your implementations.

For example, with DH you could start by executing a key exchange such that Bob and another entity X
share a secret s. At this point, you hope that X is Alice, but you can't know for sure, so you want to execute the identification protocol. You can't just execute it naively over that channel, however, because Eve could be X and she could just have created a second secure channel with Alice using secret s′
to forward all the messages between them (standard Diffie-Hellman man-in-the-middle)

We can note at this point though, that by creating the shared secret s
, Bob has already had input into the protocol. Instead of him sending a challenge y, Alice can simply use s as the challenge for the identification scheme. This has the effect of binding the secure channel and the identification scheme. A successful execution of that scheme will then guarantee that Alice is actually on the other end of the channel (since Alice's identification was tied to s, which itself constructs the channel).

The only way Eve can defeat this is if she can create two channels such that s=s′. This would allow her to "splice" the channels together, and the identification would work effectively with both channels. However, if the key exchange is contributive (s is not determined unilaterally by one party), there is no way that Eve can create two channels where s=s′.

There is something like this in pretty much every secure internet protocol, including TLS. I believe the binding value is a hash of the transcript of the protocol execution or something, but the principle is the same.
```