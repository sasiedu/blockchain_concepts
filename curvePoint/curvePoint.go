package curvePoint

import (
	"crypto/elliptic"
	"fmt"
	"math/big"
)

// CurvePoint is a struct of an ellipitc curve point
type CurvePoint struct {
	X *big.Int
	Y *big.Int
}

// Add function: Returns a CurvePoint after adding 2 CurvePoints
func (p *CurvePoint) Add(curve *elliptic.CurveParams, point *CurvePoint) *CurvePoint {
	x, y := curve.Add(p.X, p.Y, point.X, point.Y)
	return &CurvePoint{x, y}
}

// Double function: Returns a CurvePoint after doubling a CurvePoint
func (p *CurvePoint) Double(curve *elliptic.CurveParams) *CurvePoint {
	x, y := curve.Double(p.X, p.Y)
	return &CurvePoint{x, y}
}

// ScalarMult function: Returns a CurvePoint after multiplying a CurvePoint by a scalar value
func (p *CurvePoint) ScalarMult(curve *elliptic.CurveParams, scalar []byte) *CurvePoint {
	x, y := curve.ScalarMult(p.X, p.Y, scalar)
	return &CurvePoint{x, y}
}

func (p *CurvePoint) String() string {
	return fmt.Sprintf("(%s, %s)", p.X.String(), p.Y.String())
}
